#ifndef PTREV_MODULE_H
#define PTREV_MODULE_H

#include <stdio.h>
#include <stdbool.h>

#include "fileformat.h"

#ifdef __cplusplus
extern "C" {
#endif

struct PtrevModuleState {
	int logfd;
	PtrevEventId next_event_id;
};

typedef long PtrevEventHandle;

static const PtrevEventHandle ptrevNoEventHandle = 0;

bool ptrev_module_init(struct PtrevModuleState *state, const char *module_name, const char* file_name);
PtrevTimestamp ptrev_timestamp_now();
// Fills id, optionally other_event, returns handle to event and writes to file
PtrevEventHandle ptrev_event_write(struct PtrevModuleState *state, struct PtrevEvent *event, const PtrevEventHandle other_event, const void *data);
void ptrev_module_exit(struct PtrevModuleState *state);

#ifdef __cplusplus
} // extern "C"
#endif

#endif // PTREV_MODULE_H
