#include <unistd.h>
#include <fcntl.h>
#include <string.h>
#include <stdlib.h>
#include <errno.h>
#include <sys/stat.h>

#include "module.h"

/* Handles partial writes and EINTR.
 * Returns true only if count bytes were written successfully. */
static bool sane_write(int fd, const void *buf, size_t count)
{
	size_t bytes_left = count;
	const char *buf_left = (const char*)buf;
	while(bytes_left)
	{
		ssize_t step = write(fd, buf_left, bytes_left);
		if(step == -1)
		{
			if(errno == EINTR)
				continue;
			else
				return false;
		}
		else if(step == 0)
			return false;

		bytes_left -= step;
		buf_left += step;
	}

	return true;
}

/* Handles partial reads and EINTR.
 * Returns true only if count bytes were read successfully. */
static bool sane_read(int fd, void *buf, size_t count)
{
	size_t bytes_left = count;
	char *buf_left = (char*)buf;
	while(bytes_left)
	{
		ssize_t step = read(fd, buf_left, bytes_left);
		if(step == -1)
		{
			if(errno == EINTR)
				continue;
			else
				return false;
		}
		else if(step == 0)
			return false;

		bytes_left -= step;
		buf_left += step;
	}

	return true;
}

static PtrevTimestamp ptrev_timestamp_clock(int clockid)
{
	struct timespec ts;
	clock_gettime(clockid, &ts);
	return (PtrevTimestamp)(ts.tv_sec) * 1000 * 1000 + (ts.tv_nsec/1000);
}

bool ptrev_module_init(struct PtrevModuleState *state, const char *module_name, const char* file_name)
{
	const char *dirname = getenv("PTREV_LOGDIR");
	if(!dirname)
		return false;

	int dirfd = open(dirname, O_RDONLY | O_DIRECTORY | O_CLOEXEC);
	if (dirfd < 0)
		return false;

	state->logfd = openat(dirfd, file_name, O_RDWR | O_CREAT | O_EXCL | O_CLOEXEC, 0600);
	state->next_event_id = 0;

	if(state->logfd == -1 && errno == EEXIST)
	{
		// The file exists already - append events
		state->logfd = openat(dirfd, file_name, O_RDWR | O_CLOEXEC);

		close(dirfd);

		// Read the id of the last event
		if(lseek(state->logfd, sizeof(struct PtrevHeader), SEEK_SET) < 0)
		{
			close(state->logfd);
			return false;
		}

		while(true)
		{
			struct PtrevEvent event;
			if(!sane_read(state->logfd, &event, sizeof(event)))
				break;

			state->next_event_id = event.id + 1;
			if(lseek(state->logfd, event.data_size, SEEK_CUR) < 0)
			{
				close(state->logfd);
				return false;
			}
		}

		return true;
	}

	close(dirfd);

	if(state->logfd < 0)
		return false;

	struct PtrevHeader header = {
		.signature = PtrevHeaderSignature,
		.version = PtrevHeaderVersion,
		.events = 0,
		.time_monotonic_start = ptrev_timestamp_clock(CLOCK_MONOTONIC),
		.time_realtime_start = ptrev_timestamp_clock(CLOCK_REALTIME),
	};

	strncpy(header.module_name, module_name, sizeof(header.module_name));

	return sane_write(state->logfd, &header, sizeof(header));
}

PtrevTimestamp ptrev_timestamp_now()
{
	return ptrev_timestamp_clock(CLOCK_MONOTONIC);
}

static PtrevEventHandle ptrev_event_write_header(struct PtrevModuleState *state, struct PtrevEvent *event, const PtrevEventHandle other_event)
{
	event->id = state->next_event_id;
	// If writing the event fails, the id shouldn't get reused.
	state->next_event_id++;

	// Link events together?
	if(other_event != ptrevNoEventHandle)
	{
		// Read the other event's id into event's other_event
		off_t offset = (char*)&event->id - (char*)event;
		if(lseek(state->logfd, other_event + offset, SEEK_SET) < 0)
			return ptrevNoEventHandle;

		if(!sane_read(state->logfd, &event->other_event, sizeof(event->other_event)))
			return ptrevNoEventHandle;

		// Write event's id into other event's other_id
		offset = (char*)&event->other_event - (char*)event;
		if(lseek(state->logfd, other_event + offset, SEEK_SET) < 0)
			return ptrevNoEventHandle;

		if(!sane_write(state->logfd, &event->id, sizeof(event->id)))
			return ptrevNoEventHandle;
	}

	PtrevEventHandle ret = lseek(state->logfd, 0, SEEK_END);

	if(!sane_write(state->logfd, event, sizeof(*event)))
		return ptrevNoEventHandle;

	return ret;
}

PtrevEventHandle ptrev_event_write(struct PtrevModuleState *state, struct PtrevEvent *event, const PtrevEventHandle other_event, const void *data)
{
	PtrevEventHandle ret = ptrev_event_write_header(state, event, other_event);

	if(ret == ptrevNoEventHandle || !sane_write(state->logfd, data, event->data_size))
		return ptrevNoEventHandle;

	return ret;
}

void ptrev_module_exit(struct PtrevModuleState *state)
{
	struct PtrevHeader header;
	off_t offset = (char*)&header.events - (char*)&header;
	if(lseek(state->logfd, offset, SEEK_SET) == -1)
		return;

	sane_write(state->logfd, &state->next_event_id, sizeof(state->next_event_id));
	close(state->logfd);
}
