#ifndef PTREV_FILEFORMAT_H
#define PTREV_FILEFORMAT_H

#include <sys/types.h>
#include <stdint.h>
#include <time.h>

static const uint32_t PtrevHeaderSignature =
        (uint32_t)'P' << 24 | (uint32_t)'t' << 16 | (uint32_t)'r' << 8 | 'e';
static const uint32_t PtrevHeaderVersion = 0;

/* Microseconds since the unix epoch */
typedef uint64_t PtrevTimestamp;

typedef uint32_t PtrevEventId;

struct PtrevHeader {
	uint32_t signature;
	uint32_t version;
	char module_name[32];
	/* Might not be correct. */
	PtrevEventId events;
	PtrevTimestamp time_monotonic_start;
	PtrevTimestamp time_realtime_start;
};

enum PtrevEventType {
	ProcessForked,
	ProcessCreated,
	ProcessStarted,
	ProcessExited,
};

static const PtrevEventId ptrevInvalidEvent = ~0u,
                          ptrevNoEvent = ~1u;

struct PtrevEvent {
	PtrevEventId id;
	PtrevTimestamp time;
	/* If ptrevNoEvent, this is not a start/end pair.
	 * If ptrevInvalidEvent, it did not finish. */
	PtrevEventId other_event;
	pid_t pid_from;
	pid_t pid_to;
	enum PtrevEventType type;
	size_t data_size;
	char data[];
};

#endif // PTREV_FILEFORMAT_H
