#include <QDir>
#include <QDebug>

#include "ptrevlogdirmodel.h"

PtrevLogdirModel::PtrevLogdirModel(QObject *parent)
    : QAbstractItemModel(parent)
{
	connect(this, &PtrevLogdirModel::pathChanged, this, &PtrevLogdirModel::load);
}

QDateTime PtrevLogdirModel::timeForOffset(int offset)
{
	PtrevTimestamp absolute = earliestEntry->model.getRealtimeStart() + offset;
	return QDateTime::fromMSecsSinceEpoch(absolute / 1000);
}

QModelIndex PtrevLogdirModel::index(int row, int column, const QModelIndex &parent) const
{
	// List of parent's chilren
	auto &children = parent.internalPointer()
	                 ? static_cast<Entry*>(parent.internalPointer())->children
	                 : toplevelEntries;

	// Out of bounds
	if(column != 0 || row < 0 || (unsigned int) row >= children.size())
		return {};

	return createIndex(row, column, (void*)children[row].get());
}

QVariant PtrevLogdirModel::data(const QModelIndex &index, int role) const
{
	if(!index.internalPointer())
		return {};

	auto entry = (Entry*)index.internalPointer();
	PtrevLogfileModel *logfileModel = &entry->model;

	switch (role)
	{
	case LogNameRole:
		return QVariant::fromValue(logfileModel->getLogFileName());
	case ModuleNameRole:
		return QVariant::fromValue(logfileModel->getModuleName());
	case TimeOffsetRole:
		return QVariant::fromValue(static_cast<unsigned int>(logfileModel->getRealtimeStart() - earliestEntry->model.getRealtimeStart()));
	case EventModelRole:
		return QVariant::fromValue(logfileModel);
	default:
		return {};
	}
}

QHash<int, QByteArray> PtrevLogdirModel::roleNames() const
{
	QHash<int, QByteArray> roles;
	roles[LogNameRole] = "logName";
	roles[ModuleNameRole] = "moduleName";
	roles[TimeOffsetRole] = "timeOffset";
	roles[EventModelRole] = "eventModel";
	return roles;
}

QModelIndex PtrevLogdirModel::parent(const QModelIndex &index) const
{
	if(!index.internalPointer())
		return {};

	auto entry = (Entry*)index.internalPointer();
	if(entry->parent.lock() == nullptr)
		return {}; // Toplevel

	auto parentEntry = entry->parent.lock();
	// We need the index of the parent in its parent's children list
	auto grandparent = parentEntry->parent.lock();
	auto &grandparentChildren = grandparent ? grandparent->children : toplevelEntries;
	auto it = std::find(grandparentChildren.begin(), grandparentChildren.end(), parentEntry);

	if(it == grandparentChildren.end())
	{
		qWarning() << "Internal parent not found";
		return {};
	}

	return createIndex(it - grandparentChildren.begin(), 0, parentEntry.get());
}

int PtrevLogdirModel::rowCount(const QModelIndex &index) const
{
	return index.internalPointer()
	        ? static_cast<Entry*>(index.internalPointer())->children.size()
	        : toplevelEntries.size();
}

int PtrevLogdirModel::columnCount(const QModelIndex &parent) const
{
	Q_UNUSED(parent);
	return 1;
}

void PtrevLogdirModel::load()
{
	beginResetModel();

	earliestEntry = nullptr;
	toplevelEntries.clear();

	QDir logdir(path);
	logdir.setNameFilters(QStringList() << "*.ptrev");

	QStringList logfilenames = logdir.entryList();
	if(logfilenames.isEmpty())
	{
		// No *.ptrev files in path.
		endResetModel();
		return;
	}

	// Preparations for calculating the tree structure below.
	std::map<pid_t, std::shared_ptr<Entry>> pidToEntryMap;
	std::vector<std::shared_ptr<Entry>> entries;

	// Create PtrevLogfileModel instances for all *.ptrev files in path.
	for(auto &logfilename : logfilenames)
	{
		QString logfilepath = logdir.filePath(logfilename);
		auto entry = std::make_shared<Entry>(this);

		if(!entry->model.loadFromFile(logfilepath))
		{
			qWarning() << "Could not load" << logfilepath;
			continue;
		}

		qDebug() << "Loaded" << logfilepath << entry->model.getPid() << entry->model.getParentPid();

		// Find the entry with the earliest getRealtimeStart() value
		if(earliestEntry == nullptr
		   || earliestEntry->model.getRealtimeStart() > entry->model.getRealtimeStart())
			earliestEntry = entry;

		if(entry->model.getPid() != -1)
		{
			if(pidToEntryMap.find(entry->model.getPid()) != pidToEntryMap.end())
			{
				qWarning() << "Duplicate PID, ignoring" << entry->model.getLogFileName();
				continue;
			}

			pidToEntryMap[entry->model.getPid()] = entry;
		}

		entries.push_back(entry);
	}

	// Sort into a tree model by assigning each entry to its parent
	for(auto &entry : entries)
	{
		auto parentPid = entry->model.getParentPid();
		if(parentPid != -1)
		{
			if(entry->model.getPid() == parentPid)
			{
				qWarning() << entry->model.getLogFileName() << "is its own parent";
				toplevelEntries.push_back(entry);
				continue;
			}

			auto parentEntryIt = pidToEntryMap.find(parentPid);
			if(parentEntryIt != pidToEntryMap.end())
			{
				qDebug() << entry->model.getLogFileName() << "has" << parentEntryIt->second->model.getLogFileName() << "as parent";
				parentEntryIt->second->children.push_back(entry);
				entry->parent = parentEntryIt->second;
				continue;
			}
		}

		// Parent not found
		qDebug() << entry->model.getLogFileName() << "has no parent";
		toplevelEntries.push_back(entry);
	}

	endResetModel();
}
