import QtQuick 2.0

/* This shows vertical lines as markers for the X axis. */
Item {
    property int usecLeft
    property int usecPerPx

    property int usecPerDivision: {
        // At least 100px between divisions
        var min = usecPerPx * 50;
        var log10 = Math.ceil(Math.log(min) / Math.log(10));

        return Math.pow(10, log10);
    }

    property int pxPerDivision: usecPerDivision / usecPerPx
    property int offsetFirstDivision: pxPerDivision - ((usecLeft + usecPerDivision) % usecPerDivision) / usecPerPx

    Repeater {
        model: Math.ceil(parent.width / pxPerDivision)
        delegate: Rectangle {
            x: offsetFirstDivision + modelData * pxPerDivision
            y: 0
            height: parent.height
            width: 1
            color: "#bbb"
        }
    }
}
