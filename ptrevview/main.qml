import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.2
import QtQml.Models 2.1

import org.ptrev 1.0 as Ptrev

ApplicationWindow {
    id: app
    visible: true
    width: 640
    height: 480
    title: qsTr("Ptrev Viewer")

    property int usecPerPx: 10000

    // This timer fires once to set usecPerPx
    Timer {
        running: true
        interval: 10
        onTriggered: {
            // Make sure that top-level items are fully visible
            var firstEvent = 0, lastEvent = 1*1000*1000;
            for(var i = 0; i < dirModel.rowCount(); ++i)
            {
                var logfileIndex = dirModel.index(i, 0, null);
                var events = dirModel.data(logfileIndex, Ptrev.LogdirModel.EventModelRole);
                var offset = dirModel.data(logfileIndex, Ptrev.LogdirModel.TimeOffsetRole);
                if(events.rowCount() < 1)
                    continue;

                firstEvent = Math.min(firstEvent, events.data(events.index(0, 0, null), offset + Ptrev.LogfileModel.TimeOffsetRole));

                var lastEventIndex = events.rowCount() - 1;

                lastEvent = Math.max(firstEvent, events.data(events.index(lastEventIndex, 0, null), offset + Ptrev.LogfileModel.TimeOffsetRole));
            }

            app.usecPerPx = (lastEvent - firstEvent) / (scrollView.flickableItem.width - usecToX(0));
        }
    }

    function xToUsec(x) {
        return (x - processListLayout.x) * usecPerPx;
    }

    function usecToX(usec) {
        return (usec / usecPerPx) + processListLayout.x;
    }

    function usecToStr(usec) {
        if(usec >= 10*1000*1000)
            return qsTr("%1 s").arg(Number(usec / 1000*1000).toLocaleString());
        else if(usec >= 10*1000)
            return qsTr("%1 ms").arg(Number(usec / 1000).toLocaleString());

        return qsTr("%1 us").arg(Number(usec).toLocaleString());
    }

    Ptrev.LogdirModel {
        id: dirModel
        path: "/tmp/ptrevout"
        Component.onCompleted: {
            console.log("Loaded " + rowCount() + " top-level elements");
        }
    }

    // Background
    Rectangle {
        color: "white"
        anchors.fill: scrollView
    }

    TimeGrid {
        id: timeGrid
        // Behind scrollView
        anchors.fill: scrollView

        usecLeft: xToUsec(mainFlickable.contentX)
        usecPerPx: app.usecPerPx
    }

    RectangleShadow {
        item: legend
        shadowLeft: 3
        shadowTop: 3
    }

    Rectangle {
        id: legend
        parent: scrollView.flickableItem

        anchors {
            right: parent.right
            bottom: parent.bottom
        }

        width: layout.width + layout.x * 2
        height: layout.height + layout.y * 2

        radius: 4
        z: 10

        color: "white"

        ColumnLayout {
            id: layout
            x: 5
            y: 5

            Text {
                text: qsTr("Division: %1").arg(usecToStr(timeGrid.usecPerDivision))
            }
        }
    }

    ScrollView {
        id: scrollView
        anchors.fill: parent

        /* Normally this would be a ListView - but by using a Repeater in a ColumnLayout,
         * Delegates aren't unloaded which makes getting the coordinates of items in the
         * scene much easier. */
        Flickable {
            id: mainFlickable

            // Prevent artifacts
            pixelAligned: true

            contentWidth: processListLayout.width + processListLayout.x
            contentHeight: processListLayout.height

            flickableDirection: Flickable.HorizontalAndVerticalFlick

            // Show the time of the position indicator
            Text {
                parent: app.contentItem
                visible: timerect.containsMouse && timerect.mouseX >= timerect.minX && timerect.mouseX <= timerect.maxX
                x: timerect.mouseX - mainFlickable.contentX
                y: app.height - implicitHeight
                text: usecToStr(xToUsec(timerect.mouseX))
            }

            TimeRangeSelector {
                id: timerect
                anchors.fill: parent
                z: 3

                minX: processListLayout.x
                maxX: processListLayout.x + processListLayout.width

                Component.onCompleted: forceActiveFocus();

                function zoom(factor) {
                    var startUsec = xToUsec(mainFlickable.contentX);
                    var endUsec = xToUsec(mainFlickable.contentX + mainFlickable.width);

                    var centerUsec = containsMouse && (inBounds(mouseX) === mouseX)
                            ? xToUsec(mouseX) : (startUsec + endUsec) / 2;
                    var centerX = containsMouse && (inBounds(mouseX) === mouseX)
                            ? (mouseX - mainFlickable.contentX) : mainFlickable.width / 2;

                    var newUsecPerPx = (endUsec - startUsec) / mainFlickable.width;

                    newUsecPerPx *= factor;

                    newUsecPerPx = Math.max(1, Math.min(newUsecPerPx, 50000));

                    app.usecPerPx = newUsecPerPx;
                    mainFlickable.contentX = usecToX(centerUsec) - centerX;
                    mainFlickable.returnToBounds();
                }

                onWheel: {
                    if(wheel.modifiers & Qt.ControlModifier)
                    {
                        wheel.accepted = true;
                        zoom(Math.pow(0.75, Math.round(wheel.angleDelta.y / 15)));
                    }
                    else
                        wheel.accepted = false;
                }

                onRangeRequested: {
                    var startUsec = xToUsec(start);
                    var endUsec = xToUsec(end);

                    var newUsecPerPx = (endUsec - startUsec) / mainFlickable.width;
                    newUsecPerPx = Math.max(1, newUsecPerPx);

                    app.usecPerPx = newUsecPerPx;
                    mainFlickable.contentX = usecToX(startUsec);
                    mainFlickable.returnToBounds();
                }

                /* Key behaviour: on Ctrl-Minus zoom out, on Ctrl-Plus zoom in.
                 * The focus is on the current mouseX if possible, the viewport center otherwise.
                 */
                Keys.onPressed: {
                    if(event.key === Qt.Key_Minus && event.modifiers === Qt.ControlModifier)
                    {
                        zoom(2);
                        event.accepted = true;
                    }
                    else if(event.key === Qt.Key_Plus && event.modifiers === Qt.ControlModifier)
                    {
                        zoom(0.5);
                        event.accepted = true;
                    }
                    else
                        event.accepted = false;
                }
            }

            ColumnLayout {
                // It has to be a child of the overlay MouseArea to make sure that
                // the included MouseAreas work.
                parent: timerect

                id: processListLayout

                // Fill at least the main window
                height: Math.max(app.height-20, childrenRect.height)
                width: Math.max(app.width, childrenRect.width)

                // Reserve some space for the labels on the left
                x: 100

                // Drawn behind the range selector
                z: -1

                Repeater {
                    id: processListView
                    model: dirModel
                    delegate: Loader {
                        property var rootIndex: dirModel.index(index, 0, null)
                        property string logName: model.logName
                        property string moduleName: model.moduleName
                        property int timeOffset: model.timeOffset
                        property var eventModel: model.eventModel
                        property var childDelegate: sourceComponent
                        sourceComponent: ProcessDelegate {}
                    }
                }

                Item {
                    Layout.fillHeight: true
                }
            }
        }
    }
}
