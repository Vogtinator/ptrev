import QtQuick 2.11

// A rotating toggle button with an arrow in its center.
Rectangle {
    property bool pressed: false

    property string foregroundColor: "#222";
    property string backgroundColor: "#ddd";

    width: 15
    height: 15

    color: mouseArea.containsMouse ? foregroundColor : backgroundColor
    radius: width / 2
    border.width: 0
    border.color: "black"

    rotation: pressed ? 90 : 0

    Behavior on rotation {
        RotationAnimation {
            duration: 150
        }
    }

    Canvas {
        id: arrow
        width: 6
        height: 6

        anchors.centerIn: parent

        onPaint: {
            var ctx = getContext("2d");
            ctx.strokeStyle = mouseArea.containsMouse ? backgroundColor: foregroundColor;
            ctx.lineWidth = 1;
            ctx.beginPath();
            ctx.moveTo(0, 0);
            ctx.lineTo(width - 1, Math.floor(height / 2));
            ctx.lineTo(0, height - 1);
            ctx.stroke();
        }
    }

    MouseArea {
        id: mouseArea
        anchors.fill: parent
        hoverEnabled: true
        onContainsMouseChanged: arrow.requestPaint()
        onClicked: parent.pressed = !parent.pressed
    }
}
