#ifndef PTREVLOGDIRMODEL_H
#define PTREVLOGDIRMODEL_H

#include <memory>
#include <vector>

#include <QAbstractItemModel>
#include <QString>
#include <QDateTime>

#include "ptrevlogfilemodel.h"

/* This model represents the data collected in a directory.
 * The various module logfiles are managed in a tree, with the parent/child relationship
 * determined by the PtrevLogfileModel's pid() and parentPid().
 * To load the data, simply set the path property to a directory containing
 * *.ptrev files. */
class PtrevLogdirModel : public QAbstractItemModel
{
	Q_OBJECT
	Q_PROPERTY(QString path MEMBER path NOTIFY pathChanged)
public:
	enum Roles {
		LogNameRole = Qt::DisplayRole,
		ModuleNameRole = Qt::UserRole,
		TimeOffsetRole,
		EventModelRole,
	};
	Q_ENUM(Roles);

	explicit PtrevLogdirModel(QObject *parent=nullptr);

	// Use this to get the absolute date/time of a timeOffset.
	Q_INVOKABLE QDateTime timeForOffset(int offset);

	// QAbstractItemModel overrides
	QModelIndex index(int row, int column, const QModelIndex &parent=QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	QModelIndex parent(const QModelIndex &index) const override;
	int rowCount(const QModelIndex &index) const override;
	int columnCount(const QModelIndex &parent) const override;
	QHash<int,QByteArray> roleNames() const override;

signals:
	void pathChanged();

private slots:
	void load();

private:
	/* A raw pointer to the corresponding Entry at an index is used as the internalPointer
	 * for the QModelIndex. */
	struct Entry {
		explicit Entry(QObject *parent=nullptr) : model(parent) {}
		std::weak_ptr<Entry> parent;
		PtrevLogfileModel model;
		std::vector<std::shared_ptr<Entry>> children;
	};

	/* Pointer to the Entry with the earliest creation date.
	 * It's used as "time 0" for timeOffset calculation
	 * and monotonic -> realtime conversion. */
	std::shared_ptr<Entry> earliestEntry;
	std::vector<std::shared_ptr<Entry>> toplevelEntries;
	QString path;
};

#endif // PTREVLOGDIRMODEL_H
