import QtQuick 2.0

/* This component adds a shadow to the specified item. */
Rectangle {
    property var item

    property int shadowLeft: 5
    property int shadowRight: 5
    property int shadowTop: 5
    property int shadowBottom: 5

    parent: item.parent

    x: item.x - shadowLeft
    y: item.y - shadowTop
    width: item.width + shadowLeft + shadowRight
    height: item.height + shadowTop + shadowBottom

	opacity: 0.5
	color: "grey"
	radius: 5
}
