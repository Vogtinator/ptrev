#ifndef PTREVLOGFILEMODEL_H
#define PTREVLOGFILEMODEL_H

#include <vector>

#include <QAbstractItemModel>

#include "fileformat.h"

class PtrevLogfileModel : public QAbstractItemModel
{
	Q_OBJECT

public:
	enum Roles {
		TimeOffsetRole = Qt::UserRole,
		OtherIdRole,
		PidFromRole,
		PidToRole,
		TypeRole,
		DataRole,
		UserRole
	};
	Q_ENUM(Roles);

	enum EventType {
		ProcessForked = ::ProcessForked,
		ProcessCreated = ::ProcessCreated,
		ProcessStarted = ::ProcessStarted,
		ProcessExited = ::ProcessExited,
	};
	Q_ENUM(EventType);

	explicit PtrevLogfileModel(QObject *parent=nullptr)
	    : QAbstractItemModel(parent) {}

	Q_INVOKABLE bool loadFromFile(QString path);

	// Returns the index to the event with the given event's OtherId
	Q_INVOKABLE QModelIndex otherEvent(QModelIndex &event) const;

	QString getLogFileName() const
	    { return logFileName; }
	QString getModuleName() const
	    { return moduleName; }
	PtrevTimestamp getRealtimeStart() const
	    { return timeRealtimeStart; }

	// For parent/child relationships for processtracer and others.
	// Returns -1 if no relationship.
	pid_t getPid() const { return pid; }
	pid_t getParentPid() const { return parentPid; }

	// QAbstractItemModel overrides
	QModelIndex index(int row, int column, const QModelIndex &parent=QModelIndex()) const override;
	QVariant data(const QModelIndex &index, int role = Qt::DisplayRole) const override;
	QModelIndex parent(const QModelIndex &child) const override;
	int rowCount(const QModelIndex &parent) const override;
	int columnCount(const QModelIndex &parent) const override;
	QHash<int,QByteArray> roleNames() const override;

private:
	// Internal representation of a PtrevEvent
	struct Event {
		QByteArray data;
		PtrevEvent event;
	};

	bool loadFromFileInternal(QString path);

	QString logFileName, moduleName;
	pid_t pid = -1, parentPid = -1;
	std::vector<Event> events;
	std::map<uint32_t, int> idToIndexMap;
	PtrevTimestamp timeMonotonicStart;
	PtrevTimestamp timeRealtimeStart;
};

#endif // PTREVLOGFILEMODEL_H
