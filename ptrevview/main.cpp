#include <QGuiApplication>
#include <QQmlApplicationEngine>

#include "ptrevlogfilemodel.h"
#include "ptrevlogdirmodel.h"

int main(int argc, char *argv[])
{
	QCoreApplication::setAttribute(Qt::AA_EnableHighDpiScaling);

	QGuiApplication app(argc, argv);

	qmlRegisterType<PtrevLogfileModel>("org.ptrev", 1, 0, "LogfileModel");
	qmlRegisterType<PtrevLogdirModel>("org.ptrev", 1, 0, "LogdirModel");

	QQmlApplicationEngine engine;
	engine.load(QUrl(QStringLiteral("qrc:/main.qml")));
	if (engine.rootObjects().isEmpty())
		return -1;

	return app.exec();
}
