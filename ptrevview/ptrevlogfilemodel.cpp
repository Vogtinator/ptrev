#include <QFileInfo>

#include "ptrevlogfilemodel.h"

bool PtrevLogfileModel::loadFromFile(QString path)
{
	beginResetModel();

	events.clear();
	idToIndexMap.clear();

	bool ret = loadFromFileInternal(path);
	if(!ret)
	{
		events.clear();
		idToIndexMap.clear();
	}

	endResetModel();

	return ret;
}

QModelIndex PtrevLogfileModel::otherEvent(QModelIndex &event) const
{
	if(event.parent().isValid() || event.column() != 0
	   || event.row() < 0 || (unsigned int) event.row() >= events.size())
		return {};

	auto it = idToIndexMap.find(events[event.row()].event.other_event);
	if(it == idToIndexMap.end())
		return QModelIndex();

	return index(it->second, 0);
}

QModelIndex PtrevLogfileModel::index(int row, int column, const QModelIndex &parent) const
{
	if(parent.isValid() || column != 0
	   || row < 0 || (unsigned int) row >= events.size())
		return {};

	return createIndex(row, column, nullptr);
}

QVariant PtrevLogfileModel::data(const QModelIndex &index, int role) const
{
	if(index.parent().isValid() || index.column() != 0
	   || index.row() < 0 || (unsigned int) index.row() >= events.size())
		return {};

	const Event &event = events[index.row()];

	switch (role)
	{
	case TimeOffsetRole:
		return QVariant::fromValue(static_cast<unsigned int>(event.event.time - timeMonotonicStart));
	case OtherIdRole:
		return QVariant::fromValue(event.event.other_event);
	case PidFromRole:
		return QVariant::fromValue(event.event.pid_from);
	case PidToRole:
		return QVariant::fromValue(event.event.pid_to);
	case TypeRole:
		return QVariant::fromValue(static_cast<EventType>(event.event.type));
	case DataRole:
		return QVariant::fromValue(event.data);
	default:
		return {};
	}
}

QHash<int, QByteArray> PtrevLogfileModel::roleNames() const
{
	QHash<int, QByteArray> roles;
	roles[TimeOffsetRole] = "timeOffset";
	roles[OtherIdRole] = "otherId";
	roles[PidFromRole] = "pidFrom";
	roles[PidToRole] = "pidTo";
	roles[TypeRole] = "type";
	roles[DataRole] = "data";
	return roles;
}

QModelIndex PtrevLogfileModel::parent(const QModelIndex &child) const
{
	Q_UNUSED(child);
	return QModelIndex();
}

int PtrevLogfileModel::rowCount(const QModelIndex &parent) const
{
	if(parent.isValid())
		return 0;

	return events.size();
}

int PtrevLogfileModel::columnCount(const QModelIndex &parent) const
{
	if(parent.isValid())
		return 0;

	return 1;
}

bool PtrevLogfileModel::loadFromFileInternal(QString path)
{
	QFile file(path);
	if(!file.open(QIODevice::ReadOnly))
		return false;

	logFileName = QFileInfo(file).completeBaseName();

	// Read the header
	PtrevHeader header;
	if(file.read(reinterpret_cast<char*>(&header), sizeof(header)) != sizeof(header))
		return false;

	if(header.signature != PtrevHeaderSignature
	   || header.version != PtrevHeaderVersion)
		return false;

	moduleName = QString::fromUtf8(header.module_name, strnlen(header.module_name, sizeof(header.module_name)));
	timeMonotonicStart = header.time_monotonic_start;
	timeRealtimeStart = header.time_realtime_start;

	events.reserve(header.events);

	// Read all events
	while(true)
	{
		Event &event = events.emplace_back();

		if(file.read(reinterpret_cast<char*>(&event.event), sizeof(event.event)) != sizeof(event.event))
		{
			events.pop_back();
			break;
		}

		idToIndexMap[event.event.id] = events.size() - 1;

		event.data = file.read(event.event.data_size);
		if(static_cast<size_t>(event.data.size()) != event.event.data_size)
			event.data = QByteArray();
	}

	events.shrink_to_fit();

	if(moduleName == QStringLiteral("processtracer") && events.size() > 0)
	{
		if(events[0].event.type == ::ProcessForked
		   || events[0].event.type == ::ProcessCreated)
		{
			pid = events[0].event.pid_to;
			parentPid = events[0].event.pid_from;
		}
	}

	// Ignore file corruption at the end
	// return file.atEnd();

	return true;
}
