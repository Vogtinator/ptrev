import QtQuick 2.0
import QtQuick.Controls 1.0
import QtQuick.Layouts 1.2
import QtQml.Models 2.1

import org.ptrev 1.0 as Ptrev

ColumnLayout {
	Item {
		id: processRow
		
		Layout.minimumHeight: rect.implicitHeight + rect.anchors.topMargin + rect.anchors.bottomMargin
		Layout.minimumWidth: rect.anchors.leftMargin + rect.childrenRect.width
		
		Label {
			anchors {
				right: childrenExpand.visible ? childrenExpand.left : rect.left
				rightMargin: 5
				verticalCenter: parent.verticalCenter
			}
			
			text: logName
			font.pointSize: 9
		}
		
		ChildrenExpandButton {
			id: childrenExpand
			anchors {
				right: rect.left
				rightMargin: 5
				verticalCenter: parent.verticalCenter
			}
			visible: childProcessModel.count > 0
			
			pressed: true
		}
		
		property int offsetFirstEvent: eventModel.data(eventModel.index(0, 0, null), Ptrev.LogfileModel.TimeOffsetRole)
		property int offsetLastEvent: eventModel.rowCount() > 1 ? eventModel.data(eventModel.index(eventModel.rowCount() - 1, 0, null), Ptrev.LogfileModel.TimeOffsetRole) : (offsetFirstEvent + 1 * 1000 * 1000)
		
		Rectangle {
			id: rect
			implicitHeight: 20
			implicitWidth: (processRow.offsetLastEvent - processRow.offsetFirstEvent) / app.usecPerPx
			anchors {
				left: parent.left
				leftMargin: (timeOffset + processRow.offsetFirstEvent) / app.usecPerPx
				verticalCenter: parent.verticalCenter
				topMargin: 4
				bottomMargin: 4
			}
			
			radius: 8
			color: "#d5d5d5"
			border {
				color: "black"
				width: 2
			}
		}
		
		Repeater {
			model: eventModel
			delegate: Loader {
				// This Loader is like a "switch-case" statement.
				// The sourceComponent is chosen depending on the event type
				//sourceComponent: pidTo == pidFrom ? inProcessEvent : crossProcessEvent;
				
				sourceComponent: inProcessEvent
				
				// The child item sets its own parent
				visible: false
				
				Component {
					id: inProcessEvent
					Text {
						parent: rect
						anchors {
							left: parent.left
							leftMargin: (timeOffset - processRow.offsetFirstEvent) / app.usecPerPx
							verticalCenter: parent.verticalCenter
						}
						
						text: eventModel.data(eventModel.index(index, 0, null), Ptrev.LogfileModel.DataRole)
					}
				}
				
				Component {
					id: crossProcessEvent
					
					Rectangle {
						parent: mainFlickable.contentItem
						x: 300
						y: 200
						
						width: 20
						height: 20
						color: "blue"
					}
				}
			}
		}
	}

	ColumnLayout {
		id: childrenLayout
		Layout.maximumHeight: childrenExpand.pressed ? -1 : 0
		visible: childrenExpand.pressed
		
		DelegateModel {
			id: childProcessModel
            model: dirModel
            rootIndex: parent.rootIndex
            delegate: Loader {
                property var rootIndex: childProcessModel.modelIndex(index)
                property string logName: model.logName
                property string moduleName: model.moduleName
                property int timeOffset: model.timeOffset
                property var eventModel: model.eventModel
                // Implicit: property var childDelegate: childDelegate
                sourceComponent: childDelegate
			}
		}
		
		Repeater {
			model: childProcessModel
		}
	}
}
