import QtQuick 2.0

/* This component allows to select a range of X coordinates
 * with the mouse by dragging. */

MouseArea {
    id: mousearea

    signal rangeRequested(real start, real end);
    signal pointClicked(real x);

    property int minX: 0
    property int maxX: width

    property int dragStartX: 0
    property int dragStopX: 0

    property int start: Math.min(dragStartX, dragStopX)
    property int end: Math.max(dragStartX, dragStopX)

    acceptedButtons: Qt.LeftButton
    hoverEnabled: true

    function inBounds(x) {
        return Math.max(minX, Math.min(x, maxX));
    }

    onMouseXChanged: {
        if(pressed)
            dragStopX = inBounds(mouseX);
        else
            dragStartX = dragStopX = inBounds(mouseX);
    }

    onReleased: {
        if(end - start < 5)
            pointClicked(start)
        else
            rangeRequested(start, end)

        dragStartX = dragStopX = inBounds(mouseX);
    }

    Rectangle {
        id: indicator

        visible: {
            if(mousearea.pressed)
                return true;

            return (mousearea.containsMouse && mousearea.mouseX >= minX && mousearea.mouseX <= maxX);
        }

        anchors {
            top: parent.top
            bottom: parent.bottom
        }

        x: mousearea.start
        width: mousearea.end - mousearea.start + 1

        color: "#40000000"
        antialiasing: false
    }
}
