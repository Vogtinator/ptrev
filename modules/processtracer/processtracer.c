#define _GNU_SOURCE

#include <sys/types.h>
#include <unistd.h>
#include <string.h>
#include <dlfcn.h>

#include "module.h"

static struct PtrevModuleState ptrev_processtracer_state;

static PtrevEventHandle startup_handle;

static bool init_logfile()
{
	char file_name[32];
	snprintf(file_name, sizeof(file_name), "pid%d.ptrev", getpid());
	return ptrev_module_init(&ptrev_processtracer_state, "processtracer", file_name);
}

pid_t fork()
{
	typeof(&fork) real_fork = dlsym(RTLD_NEXT, "fork");

	pid_t ret = real_fork();
	if(ret == 0)
	{
		if(!init_logfile())
			return ret;

		struct PtrevEvent event = {
			.time = ptrev_timestamp_now(),
			.pid_from = getppid(),
			.pid_to = getpid(),
			.type = ProcessForked,
			.other_event = ptrevNoEvent,
			.data_size = 0,
		};

		ptrev_event_write(&ptrev_processtracer_state, &event, ptrevNoEventHandle, NULL);
	}

	return ret;
}

static __attribute__((constructor)) void ptrev_processtracer_constructor(int argc, char **argv)
{
	if(!init_logfile())
		return;

	char cmdline[128];
	memset(cmdline, 0, sizeof(cmdline));
	for(int i = 0; i < argc; ++i)
	{
		strncat(cmdline, " ", sizeof(cmdline) - strlen(cmdline) - 1);
		strncat(cmdline, argv[i], sizeof(cmdline) - strlen(cmdline) - 1);
	}

	struct PtrevEvent event = {
		.time = ptrev_timestamp_now(),
		.pid_from = getppid(),
		.pid_to = getpid(),
		.type = ProcessCreated,
		.other_event = ptrevNoEvent,
		.data_size = strlen(cmdline),
	};

	startup_handle = ptrev_event_write(&ptrev_processtracer_state, &event, ptrevNoEventHandle, cmdline);
}

static void init()
{
	struct PtrevEvent event = {
		.time = ptrev_timestamp_now(),
		.pid_from = getpid(),
		.pid_to = getpid(),
		.type = ProcessStarted,
		.other_event = ptrevInvalidEvent,
		.data_size = 0,
	};

	ptrev_event_write(&ptrev_processtracer_state, &event, startup_handle, NULL);
}

__attribute__((section(".init_array"))) typeof(init) *__init = init;

static __attribute__((destructor)) void ptrev_processtracer_destructor()
{
	struct PtrevEvent event = {
		.time = ptrev_timestamp_now(),
		.pid_from = getpid(),
		.pid_to = getpid(),
		.type = ProcessExited,
		.other_event = ptrevNoEvent,
		.data_size = 0,
	};

	ptrev_event_write(&ptrev_processtracer_state, &event, ptrevNoEventHandle, NULL);

	ptrev_module_exit(&ptrev_processtracer_state);
}
